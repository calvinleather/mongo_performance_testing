import os
import papermill as pm
import scrapbook as sb
import time

for use_antecedent_index in [True, False]:
    for n_docs in [1000, 10000, 100000,1000000]:
        for n_antecedent_length in [3,8,15]:
            for cart_size in [4,8,20]:
                print(use_antecedent_index, n_docs, n_antecedent_length, cart_size)
                pm.execute_notebook(
                   'query_test.ipynb',
                   f'output/query_test{time.time()}.ipynb',
                   parameters = dict(n_docs=n_docs, n_antecedent_length=n_antecedent_length, cart_size = cart_size, use_antecedent_index=use_antecedent_index)
                )
